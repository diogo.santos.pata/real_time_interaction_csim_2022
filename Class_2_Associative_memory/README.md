# "Associative memory" synthetic agent

<h3> In this directory you will find the implementation of the "Associative memory" synthetic agent. </h3>
<p> As seen in class, we will take the neuroscientific insights of associative memory in the mammalian hippocampus and implement its computational 
counterpart using a simple machine learning model.</p>
<p> In this folder, you will find 2 implementations of the same model, one using ml5 and the other tensorflow.js. Both of them should perform equally well. </p>
