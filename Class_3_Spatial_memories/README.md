This synthetic agent learns to reconstruct the input vector of sensory landmarks shown together in different locations.
After learning, we look at the activity of "neurons" in the hidden layer of this network, and find out some of them are active only when one specific landmark distribution is presented. Thus, we call them place-cells.

<img src="place_cell_AE.png">
