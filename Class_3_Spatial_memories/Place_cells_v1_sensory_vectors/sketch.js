var model;
var model_hidden;
var n_objects = 20;
var xs;
var ys;

var data_to_train = [];

var d1 = [1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
var d2 = [0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0];
var d3 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1];


function setup() {

  createCanvas(400, 400);

  button = createButton("d1");
  button.mousePressed(test_d1);

  button = createButton("d2");
  button.mousePressed(test_d2);

  button = createButton("d3");
  button.mousePressed(test_d3);


  add_data();
  create_model();
  train_model();
}


function add_data() {

  for (var i = 0; i < 100; i++) {
    data_to_train.push( d1 );
    data_to_train.push( d2 );
    data_to_train.push( d3 );
  }

}



function test_d1() { predict_model(d1); }
function test_d2() { predict_model(d2); }
function test_d3() { predict_model(d3); }



function predict_model(distances) {

  tmp = model_hidden.predict( tf.tensor2d( [distances] )   ).dataSync();

  var place_cells_data = []
  for (var i = 0; i < tmp.length; i++) {
    place_cells_data.push(  { index: i, value: tmp[i] } );
  }
  place_cells_data.push(  { index: "x", value: 1 } );
  // Render to visor
  const place_cells_surface = { name: 'place cells', tab: 'Charts' };
  tfvis.render.barchart(place_cells_surface, place_cells_data, {xLabel:"Cell number"} );

}



function create_model() {

  model = tf.sequential()

  model.add(tf.layers.dense({units:n_objects, activation: 'sigmoid', inputShape:n_objects, }))
  model.add(tf.layers.dense({units:Math.round(n_objects/2), activation: 'sigmoid'}))
  model.add(tf.layers.dense({units:Math.round(n_objects/4), activation: 'sigmoid'}))
  model.add(tf.layers.dense({units:Math.round(n_objects/2), activation: 'sigmoid'}))
  model.add(tf.layers.dense({units:n_objects, activation: 'sigmoid'}))

  model.compile({ optimizer: tf.train.adam(learningRate=0.05),
                  loss: 'meanSquaredError',
                  metrics: ['accuracy'] })

  const surface = { name: 'Model Summary', tab: 'Model Inspection'};
  tfvis.show.modelSummary(surface, model);

}




function train_model() {

    xs = tf.tensor2d( data_to_train );
    ys = tf.tensor2d( data_to_train );

    // Train
    const surface_history = { name: 'show.history', tab: 'Training' };
    const history = [];

    model.fit(xs, ys, {
        epochs: 50,
        callbacks: {
          onEpochEnd: (epoch, log) => { history.push(log); }
        }
        }).then(() => {
            console.log("Model trained");
            tfvis.show.history(surface_history, history, ['loss']);

            model_hidden = tf.model({inputs:model.inputs, outputs: model.layers[2].output});
          });
}
