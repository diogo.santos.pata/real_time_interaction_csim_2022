var model;
var model_hidden;

var xs;
var ys;

var rat_position = 200;
var rat_direction = 4;
var navigation_isOn = false;

var n_objects = 20;
var objects_positions;
var objects_colors;

var add_data_flag = false;
var data_to_train = [];

var log_activity = [];
var log_position = [];

var predictions_flag = false;

function setup() {

  createCanvas(400,400);
  background(153);

  navigation_button = createButton("navigate");
  navigation_button.mousePressed(activate_navigation);

  add_data_button = createButton("add data");
  add_data_button.mousePressed(add_data_to_model);

  train_model_button = createButton("train model");
  train_model_button.mousePressed(create_train_model);

  predict_button = createButton("predict");
  predict_button.mousePressed(switch_prediction);

  create_environment();

}


function switch_prediction() {
  if( predictions_flag ){ predictions_flag = false;}
  else{ predictions_flag = true; }
}


function create_train_model() {
  create_model();
  train_model();
}


function add_data_to_model() {
  if( add_data_flag ){ add_data_flag=false; }
  else{ add_data_flag=true; }
}


function predict_model() {
  distances = compute_distance_to_objects();
  tmp = model_hidden.predict( tf.tensor2d( [distances] )   ).dataSync();

  // model_hidden.predict( tf.tensor2d( [distances] )   ).print();

  var place_cells_data = []
  for (var i = 0; i < tmp.length; i++) {
    place_cells_data.push(  { index: i, value: tmp[i] } );
  }
  place_cells_data.push(  { index: "x", value: 1 } );
  // Render to visor
  const place_cells_surface = { name: 'place cells', tab: 'Charts' };
  tfvis.render.barchart(place_cells_surface, place_cells_data, {xLabel:"Cell number"} );




}


function activate_navigation() {
  if( navigation_isOn ){ navigation_isOn=false; }
  else{ navigation_isOn=true; }
}




function create_environment() {
  objects_positions = {}
  objects_colors = {}
  for (var i = 0; i < n_objects; i++) {
    objects_positions[i] = Math.round( Math.random()*400 );
    objects_colors[i] = color(random(255),random(255),random(255));
  }
}


function draw_environment() {
  for (var i = 0; i < Object.keys(objects_positions).length; i++) {
    fill(objects_colors[i]);
    rect( objects_positions[i], 100, 15,30);
  }
}




function compute_distance_to_objects(){
  distances = [];
  for (var i = 0; i < Object.keys(objects_positions).length; i++) {
    this_obj_distance =  Math.abs( rat_position - objects_positions[i] );
    if(this_obj_distance<=20){this_obj_distance=1;}
    else{this_obj_distance=0;}
    distances.push(  this_obj_distance );
  }
  return distances;
}




function draw() {
  if(navigation_isOn){
    // background(153);
    fill(153);
    rect(0,180,400,50)
    fill(255,0,0);
    ellipse( rat_position, 200, 30,30 );
    rat_position = rat_position + rat_direction;
    if( rat_position>390 || rat_position<10 ){ rat_direction = -rat_direction  }
    draw_environment();
  }

  if(predictions_flag){
    predict_model();
  }

  if(add_data_flag){
    distances = compute_distance_to_objects();
    data_to_train.push(distances);
    console.log(data_to_train.length);
  }
}





function create_model() {

  model = tf.sequential()

  model.add(tf.layers.dense({units:n_objects, activation: 'sigmoid', inputShape:n_objects, }))
  model.add(tf.layers.dense({units:Math.round(n_objects/2), activation: 'sigmoid'}))
  model.add(tf.layers.dense({units:Math.round(n_objects/4), activation: 'sigmoid'}))
  model.add(tf.layers.dense({units:Math.round(n_objects/2), activation: 'sigmoid'}))
  model.add(tf.layers.dense({units:n_objects, activation: 'sigmoid'}))

  model.compile({ optimizer: tf.train.adam(learningRate=0.05),
                  loss: 'meanSquaredError',
                  metrics: ['accuracy'] })

  const surface = { name: 'Model Summary', tab: 'Model Inspection'};
  tfvis.show.modelSummary(surface, model);

}





function train_model() {

    xs = tf.tensor2d( data_to_train );
    ys = tf.tensor2d( data_to_train );

    // Train
    const surface_history = { name: 'show.history', tab: 'Training' };
    const history = [];

    model.fit(xs, ys, {
        epochs: 50,
        callbacks: {
          onEpochEnd: (epoch, log) => { history.push(log); }
        }
        }).then(() => {
            console.log("Model trained");
            tfvis.show.history(surface_history, history, ['loss']);
            //tmp = tf.round( model.predict( tf.tensor2d( [[0,1,0,0]] )  ) ).dataSync();
            //console.log(tmp);
            model_hidden = tf.model({inputs:model.inputs, outputs: model.layers[2].output});
            //tmp_hidden =  model_hidden.predict( tf.tensor2d( [[0,0,0,1]] )   ).dataSync();
            //console.log(tmp_hidden);
          });

}
