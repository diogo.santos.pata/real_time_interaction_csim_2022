let model;
let predicted_output;

function setup() {

  createCanvas(400, 400);

  let options = {
      // inputs: 4,
      // hiddenUnits: 3,
      // outputs: 4,
      learningRate: 0.25,
      task:'regression',
      debug:true,

      layers: [
        { type: 'dense', units: 4, activation: 'sigmoid' },
        { type: 'dense', units: 3, activation: 'sigmoid' },
        { type: 'dense', units: 4, activation: 'sigmoid' }
      ]

    }
    model = ml5.neuralNetwork(options);

    model.addData([0, 1, 0, 1], [0, 1, 0, 1]);
    model.addData([1, 0, 1, 0], [1, 0, 1, 0]);
    model.addData([0, 1, 0, 1], [0, 1, 0, 1]);
    model.addData([1, 0, 1, 0], [1, 0, 1, 0]);
    model.addData([0, 1, 0, 1], [0, 1, 0, 1]);
    model.addData([1, 0, 1, 0], [1, 0, 1, 0]);

    model.train({ epochs: 50 }, finishedTraining);

    input = createInput();
    input.position(20, 30);
    button = createButton("test");
    button.position(160, 30);
    button.mousePressed(test_recall);
}

function test_recall(){
  let xs = input.value().split(",").map(Number);
  console.log(xs);
  model.predict(xs, gotResults);
}

function finishedTraining(){
  console.log('done!');
}

function gotResults(error, results){
  predicted_output = results;
  predicted_output.forEach( element =>
    console.log(element.label, Math.round(element.value) ) );
}
