var model = tf.sequential()
model.add(tf.layers.dense({units:4, activation: 'sigmoid', inputShape:4, }))
model.add(tf.layers.dense({units:3, activation: 'sigmoid'}))
model.add(tf.layers.dense({units:4, activation: 'sigmoid'}))

model.compile({ optimizer: tf.train.adam(learningRate=0.25),
                loss: 'meanSquaredError',
                metrics: ['accuracy'] })

const surface = { name: 'Model Summary', tab: 'Model Inspection'};
tfvis.show.modelSummary(surface, model);


// Prepare training data
xs = tf.tensor2d([  [1,0,1,0],
                    [0,1,0,1],
                    [1,0,1,0],
                    [0,1,0,1] ])

ys = tf.tensor2d([  [1,0,1,0],
                    [0,1,0,1],
                    [1,0,1,0],
                    [0,1,0,1] ])

// Train
const surface2 = { name: 'show.history', tab: 'Training' };
const history = [];

model.fit(xs, ys, {
    epochs: 50,
    callbacks: {
      onEpochEnd: (epoch, log) => { history.push(log); }
    }


    }).then(() => {
        console.log("done");
        tfvis.show.history(surface2, history, ['loss']);
        tmp = tf.round( model.predict( tf.tensor2d( [[1,0,0,0]] )  ) ).dataSync();
        console.log(tmp);
      });
